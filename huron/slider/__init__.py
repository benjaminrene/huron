"""

.. module:: slider
   :platform: Unix
   :synopsis: Really simple slideshow application for Django

.. moduleauthor:: Thomas Durey <tominardi@gmail.com>

This application provides :

* Administration
* Simple templates

How to install
==============

Add *huron.slider* on your INSTALLED_APPS.

Run *syncdb*.

You could now add your slideshows in your views.

A really simple template, based on jQuery Nivo Slider, comes with the
application

"""
