
"""

.. module:: contact_form
   :platform: Unix
   :synopsis: Really simple contact form application for Django

.. moduleauthor:: Thomas Durey <tominardi@gmail.com>

This application provides :

* A simple contact form
* A basic view to use it

This is a fork of https://github.com/madisona/django-contact-form

"""