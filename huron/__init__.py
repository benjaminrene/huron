# -*- coding: utf-8 -*-
"""Library of Django applications

.. moduleauthor:: Thomas Durey <tominardi@gmail.com>


"""
def get_version():
    return '0.5.4'
