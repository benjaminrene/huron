"""

.. module:: settings
   :platform: Unix
   :synopsis: Configuration for Huron Websites

.. moduleauthor:: Thomas Durey <tominardi@gmail.com>

This application provides :

* Settings objects
* A really simple template tag
* Administration

"""