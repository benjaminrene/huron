from huron.blog.models import Post, Category
from django.contrib import admin
from huron.utils.admin import CKEditorAdmin


class PostAdmin(CKEditorAdmin):
    fields = ('date_pub', 'published', 'title', 'image', 'slug', 'short_desc',
              'article', 'categories')
    list_display = ('title', 'author', 'last_editor')
    list_filter = ('date_pub', 'categories')
    search_fields = ['title', 'author']
    prepopulated_fields = {'slug': ('title',)}

    def save_model(self, request, obj, form, change):
        instance = form.save(commit=False)
        if not hasattr(instance,'author'):
            instance.author = request.user
        instance.last_editor = request.user
        instance.save()
        form.save_m2m()
        return instance

    def save_formset(self, request, form, formset, change):
        def set_user(instance):
            if not instance.author:
                instance.author = request.user
            instance.last_editor = request.user
            instance.save()

        if formset.model == Post:
            instances = formset.save(commit=False)
            map(set_user, instances)
            formset.save_m2m()
            return instances
        else:
            return formset.save()


class CategoryAdmin(CKEditorAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoryAdmin)
