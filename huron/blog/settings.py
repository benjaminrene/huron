# -- coding:utf-8 --
"""
    Default settings for huron.blog app
"""
from django.conf import settings


if hasattr(settings, 'HURON_BLOG_EXCERPT_LENGTH'):
    HURON_BLOG_EXCERPT_LENGTH = settings.HURON_BLOG_EXCERPT_LENGTH
else:
    HURON_BLOG_EXCERPT_LENGTH = 40
