How to contribute
*****************

You can find the repository on Bitbucket :

https://bitbucket.org/tominardi/huron/

Feel free to clone it and send pull requests.

Commits
=======

Your commits could look like this :

::

    [app][commit_nature]#ticket Your message

With *app* the name of the application you change, #ticket the number of the
ticket in the issue tracker, and *commit_nature* one of the following keyword :

* new
* fix
* templating
* i18n
* config
* doc
* miscellaneous


Style Guide
===========

Like every Django project, Huron is a Pep8 project.
Read http://www.python.org/dev/peps/pep-0008/ before your first commit.

naming
------

* Dates : Django doesn't have naming conventions for dates. In Huron, use this.

 * Creation date : date_rec
 * Publication date : date_pub
 * Last Edition date : date_last_edit
 * End of publication date : date_unpub
