How to use Huron
****************

Install Huron with pip::

    pip install huron

Alternatively, you can download the zip file and install with::

    python setup.py install

Then, add **huron.utils** on your INSTALLED_APPS.

You can now add one or many Huron Apps on your Django project.
