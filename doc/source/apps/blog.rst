Documentation for :py:mod:`huron.blog`
**************************************

:py:mod:`huron.blog` is a Django application to create easily a weblog.

.. automodule:: huron.blog
   :members:

How to install
==============

You need to have :py:mod:`huron.utils` installed.

If you want to be able to add images on your articles, install `huron.simple_media_manager`.

Then, add *huron.blog* on your INSTALLED_APPS.

Add this line on your URL patterns :
::

    url(r'^blog/', include("huron.blog.urls", namespace="blog")),

Of course, you can change your root pattern. Namespace should be blog

Run *syncdb*.

Settings
========

* HURON_BLOG_EXCERPT_LENGTH : number of words to print in the post listings
                              when there is no short description.

Show images during development
==============================

If you want to serve images during development, you can follow the Django Instructions `https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user` and add::

    from django.conf import settings
    from django.conf.urls.static import static

    urlpatterns = patterns('',
        # ... the rest of your URLconf goes here ...
    ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

on your urls.py file.

Http Response
=============

.. automodule:: huron.blog.views
	:members:

Model
=====

.. automodule:: huron.blog.models
    :members:
    :exclude-members: save
