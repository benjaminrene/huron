Documentation for :py:mod:`huron.menus_manager`
***********************************************

:py:mod:`huron.menus_manager` is a Django application to easily add some
menus on your website

.. automodule:: huron.menus_manager
    :members:

How to install
==============

You need to have :py:mod:`huron.utils` installed.

Then, add *huron.menus_manager* on your INSTALLED_APPS.

Add theses lines on your urls.py file :
::
    from huron.menus_manager.urls import menupatterns
    urlpatterns += menupatterns

Then, if you are on Django 1.7+ run:
::
    ./manage migrate

for Django <= 1.6:
::
    ./manage syncdb

Managing menus
==============

When you create a menu, before the first save, you can't add any elements on
it. You'll have four fields :

* Menu name
* Reference : it's a slug, used to call the menu on the views and context
              managers
* CSS identifier (optionnal) : the CSS #id for this menu
* CSS classes (optionnal) : some CSS .class for this menu

After first save, you can create some items at the bottom of the page.
You can edit hierarchies by drag-n-drops on the items.

Import menus on views
=====================

If you need to get a menu for a single view, you can do it by importing the
model on it and use the manager to get it by his slug. Then, you can add it on
the context.
::
    from huron.menus_manager.models import Menu
    contextual_menu = Menu.objects.get(slug='contextual-menu')

Note that that code will fail if the menu is not created. You should use a
try/except.

Add menus on a context processor
================================

For menus loaded in every pages (like your main menu by example), you can
create a context processor. By example, in a file named my_ctx_prcs.py

::
    from huron.menus_manager.models import Menu

    def main_menu(request):
        return {'menu' : Menu.objects.get(slug='main-menu')}

Note that that code will fail if the menu is not created. You should use a
try/except.

Add you have to add it to your settings.py::
    TEMPLATE_CONTEXT_PROCESSORS += ("my_app.my_ctx_prcs.main_menu",)

Template
========

There is a simple template (menu/menu.html) that you can use by a base for your
menus.
::
    {% with menu=contextual_menu %}
    {% include 'menu/menu.html' %}
    {% endwith %}
