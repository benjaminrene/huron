Documentation for :py:mod:`huron.simple_medias_manager`
*******************************************************

:py:mod:`huron.simple_medias_manager` is a Django application to manage images
and files on your website.


.. automodule:: huron.simple_medias_manager
   :members: 
