Documentation for :py:mod:`huron.pages`
***************************************

:py:mod:`huron.pages` is a Django application handling pages contents.

You need to have :py:mod:`huron.utils` installed.

If you want to be able to add images on your pages, install `huron.simple_media_manager`.

Then, add *huron.pages* on your INSTALLED_APPS.

Add this line on your URL patterns :
::

    url(r'^page/', include("huron.pages.urls", namespace="pages")),

Of course, you can change your root pattern. Namespace should be pages

Run *syncdb*.

Show images during development
==============================

If you want to serve images during development, you can follow the Django Instructions `https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user` and add::

    from django.conf import settings
    from django.conf.urls.static import static

    urlpatterns = patterns('',
        # ... the rest of your URLconf goes here ...
    ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

on your urls.py file.

Http Response
=============

.. automodule:: huron.pages.views
    :members:

Model
=====

.. automodule:: huron.pages.models
    :members:
    :exclude-members: save, get_publishe_page, get_permalink

