Documentation for :py:mod:`huron.simple_news`
*********************************************

:py:mod:`huron.simple_news` is a Django application to publish news.


.. automodule:: huron.simple_news
   :members: get_latest_news, get_latest_news_by_category, get_new_by_slug

Model
=====

.. automodule:: huron.simple_news.models
    :members:
    :exclude-members: save

