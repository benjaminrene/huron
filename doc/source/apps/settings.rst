Documentation for :py:mod:`huron.settings`
******************************************

:py:mod:`huron.settings` is a Django application which store key/values in
database.

.. automodule:: huron.settings
    :members: 

Model
=====

.. automodule:: huron.settings.models
    :members:

Context Processor
=================

.. automodule:: huron.settings.context_processors
    :members:

Template Tags
=============

.. automodule:: huron.settings.templatetags.settings_custom
    :members:

