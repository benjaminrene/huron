Documentation for :py:mod:`huron.slider`
****************************************

:py:mod:`huron.slider` is a Django application to create and manage Slideshows.


.. automodule:: huron.slider
   :members:

