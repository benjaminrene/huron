Documentation for :py:mod:`huron.contact_form`
**********************************************

:py:mod:`huron.contact_form` is a Django application to add a really simple
contact form in your website.

.. automodule:: huron.contact_form
    :members:

How to install
==============

You need to have :py:mod:`huron.utils` installed.

If you want to be able to add images on your articles, install `huron.simple_media_manager`.

Then, add *huron.contact_form* on your INSTALLED_APPS.

Add this line on your URL patterns :
::

    url(r'^contact/', include("huron.contact_form.urls",
        namespace="contact_form")),

Of course, you can change your root pattern. Namespace should be contact

Override Templates
==================

There is four templates to override:

* contact_form/contact.html
* contact_form/contact_completed.html
* contact_form/email_subject.html
* contact_form/email_template.html
