.. huron documentation master file, created by
   sphinx-quickstart on Wed Jul 17 15:50:26 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Huron's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 1
   :glob:

   basic/*
   apps/*
