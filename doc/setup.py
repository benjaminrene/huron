from setuptools import setup

setup(name='huron',
      version='0.5',
      description="Tools for Django websites",
      long_description="",
      author='Thomas Durey',
      author_email='tominardi@gmail.com',
      license='TODO',
      packages=['huron'],
      zip_safe=False,
      )