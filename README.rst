HURON is a toolbox for the web framework Django. It provides a lot of
utilities to make quickly your websites.

Huron works with Django 1.5 and more.

You can find some helps at :

* http://huron.readthedocs.org/en/latest/
* https://bitbucket.org/tominardi/huron

If you find some bugs, you can report them at :

* https://bitbucket.org/tominardi/huron/issues

To contact the author, please send an email to :

* tominardi@gmail.com
